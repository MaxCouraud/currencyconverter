# Currency Converter

![Symfony](https://blog.nicolashachet.com/wp-content/uploads/2014/04/symfony2.png)
### Installation environnement dev sur Linux

Tout d'abord télécharger lamp ce qui comprends Apache, PHP et Mysql
```sh
$ sudo apt install apache2 php libapache2-mod-php mysql-server php-mysql
```
Intaller composer
```sh
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('sha384', 'composer-setup.php') === 'c31c1e292ad7be5f49291169c0ac8f683499edddcfd4e42232982d0fd193004208a58ff6f353fde0012d35fdd72bc394') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
```
Intaller node
```sh
$ sudo apt-get install nodejs npm
```

### Mettre en place le projet

Intaller les dépendances de composer une fois le projet installer
```sh
$ composer install
```
A chaques fois que vous modififiez le CSS ou JS et au premier lancement du projet
```sh
$ npm run build
```
Pour développer vous pouvez lancer un petit server local
```sh
$ npm run build
```
Configurer la base de donnée dans le .env, logiquement
Le user / mot de passe sont les même si vous etes sur linux
Ici j'ai décidé d'appeler ma bdd currency mais vous pouvez changer
Dans ce contexte, c'est une base de donnée MYSQL
```sh
DATABASE_URL=mysql://root:root@127.0.0.1:3306/currency?serverVersion=5.7
```
Afin de créer la base de donnée à ce nom précis
```sh
$ php bin/console doctrine:database:create
```
Afin de créer mettre à jour la BDD
```sh
$ php bin/console d:s:u --force
```
Pour développer vous pouvez maintenant lancer un petit serveur local grâce à la commande
```sh
$ php -S localhost:8080 -t public
```
