<?php

namespace App\Repository;

use App\Entity\CurrencyOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CurrencyOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrencyOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrencyOperation[]    findAll()
 * @method CurrencyOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyOperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CurrencyOperation::class);
    }

    public function findOperationsByDay($dateMin, $dateMax)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.createdAt >= :dateMax')
            ->andWhere('c.createdAt <= :dateMin')
            ->setParameter('dateMin', $dateMin)
            ->setParameter('dateMax', $dateMax)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
