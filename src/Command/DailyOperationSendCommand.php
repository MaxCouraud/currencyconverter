<?php

namespace App\Command;

use App\Repository\CurrencyOperationRepository;
use App\Service\Mailer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:daily-operation:send',
    description: 'Add a short description for your command',
)]
class DailyOperationSendCommand extends Command
{
    private CurrencyOperationRepository $repository;
    private Mailer $mailer;

    public function __construct(
        Mailer $mailer,
        CurrencyOperationRepository $repository,
        string $name = null)
    {
        parent::__construct($name);
        $this->repository = $repository;
        $this->mailer = $mailer;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Send daily operations to customer')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $maxDate = new \DateTimeImmutable();
        $minDate = date("Y-m-d", strtotime("-1 days"));

        $operations = $this->repository->findOperationsByDay($minDate, $maxDate);

        $this->mailer->index($operations);

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
