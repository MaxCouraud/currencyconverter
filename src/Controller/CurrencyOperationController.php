<?php

namespace App\Controller;

use App\Entity\CurrencyOperation;
use App\Form\CurrencyOperationType;
use App\Repository\CurrencyOperationRepository;
use App\Service\CurrencyConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/currency/operation')]
class CurrencyOperationController extends AbstractController
{

    #[Route('/', name: 'currency_operation_index', methods: ['GET'])]
    public function index(CurrencyOperationRepository $currencyOperationRepository): Response
    {
        return $this->render('currency_operation/index.html.twig', [
            'currency_operations' => $currencyOperationRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'currency_operation_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CurrencyConverter $converter): Response
    {
        $currencyOperation = new CurrencyOperation();
        $form = $this->createForm(CurrencyOperationType::class, $currencyOperation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $currencyOperation->setResult($converter->defineOperationResult($currencyOperation));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($currencyOperation);
            $entityManager->flush();

            return $this->redirectToRoute('currency_operation_index');
        }
        return $this->render('currency_operation/new.html.twig', [
            'currency_operation' => $currencyOperation,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'currency_operation_show', methods: ['GET'])]
    public function show(CurrencyOperation $currencyOperation): Response
    {
        return $this->render('currency_operation/show.html.twig', [
            'currency_operation' => $currencyOperation,
        ]);
    }

    #[Route('/{id}/edit', name: 'currency_operation_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CurrencyOperation $currencyOperation): Response
    {
        $form = $this->createForm(CurrencyOperationType::class, $currencyOperation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('currency_operation_index');
        }

        return $this->render('currency_operation/edit.html.twig', [
            'currency_operation' => $currencyOperation,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'currency_operation_delete', methods: ['POST'])]
    public function delete(Request $request, CurrencyOperation $currencyOperation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$currencyOperation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($currencyOperation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('currency_operation_index');
    }
}
