<?php

namespace App\Form;

use App\Entity\CurrencyOperation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CurrencyOperationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstUnit')
            ->add('firstCurrency', ChoiceType::class, [
                'choices'  => [
                    'dollar' => 'dollar',
                    'euro' => 'euro',
                ]
            ])
            ->add('secondUnit')
            ->add('secondCurrency',  ChoiceType::class, [
                'choices'  => [
                    'dollar' => 'dollar',
                    'euro' => 'euro',
                ]
            ])
            ->add('resultCurrency', ChoiceType::class, [
                'choices'  => [
                    'dollar' => 'dollar',
                    'euro' => 'euro',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CurrencyOperation::class,
        ]);
    }
}
