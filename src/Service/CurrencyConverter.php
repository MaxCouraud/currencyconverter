<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\CurrencyOperation;

class CurrencyConverter
{
    private static $euro_conversion_rate = 0.84;
    private static $dollar_conversion_rate = 1.26;



    public function defineOperationResult(CurrencyOperation $currencyOperation): int
    {
        if($currencyOperation->getResultCurrency() == "dollar" && $currencyOperation->getFirstCurrency() == "euro")
        {
            $currencyOperation->setFirstUnit($this->DollarToEuroConverter($currencyOperation->getFirstUnit()));
        }
        if($currencyOperation->getResultCurrency()  == "dollar" && $currencyOperation->getSecondCurrency() == "euro")
        {
            $currencyOperation->setSecondUnit($this->DollarToEuroConverter($currencyOperation->getSecondUnit()));
        }
        if($currencyOperation->getResultCurrency()  == "euro" && $currencyOperation->getFirstCurrency() == "dollar")
        {
            $currencyOperation->setFirstUnit( $this->EuroToDollarConverter($currencyOperation->getFirstUnit()));
        }
        if($currencyOperation->getResultCurrency() == "euro" && $currencyOperation->getSecondCurrency() == "dollar")
        {
            $currencyOperation->setSecondUnit($this->EuroToDollarConverter($currencyOperation->getSecondUnit()));
        }
        return $currencyOperation->getFirstUnit()+$currencyOperation->getSecondUnit();
    }

    public function DollarToEuroConverter(int $unit) : int
    {
        return intval($unit * self::$euro_conversion_rate);
    }

    public function EuroToDollarConverter(int $unit) : int
    {
        return intval($unit * self::$dollar_conversion_rate);
    }
}
