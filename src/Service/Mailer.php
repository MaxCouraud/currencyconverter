<?php

declare(strict_types=1);

namespace App\Service;

use Swift_Mailer;
use Swift_Message;

class Mailer
{
    private Swift_Mailer $mailer;

    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function index($operations): void
    {
        $message = (new Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo('recipient@example.com')
            ->setBody(
                $this->renderView(
                    'currency_operation/index.html.twig',
                    ['currency_operation' => $operations]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);
    }
}
