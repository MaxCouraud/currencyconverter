<?php

namespace App\Entity;

use App\Repository\CurrencyOperationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyOperationRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class CurrencyOperation
{
    use BaseEntity;

    /**
     * @ORM\Column(type="integer")
     */
    private $firstUnit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstCurrency;

    /**
     * @ORM\Column(type="integer")
     */
    private $secondUnit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondCurrency;

    /**
     * @ORM\Column(type="integer")
     */
    private $result;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resultCurrency;


    public function getFirstUnit(): ?int
    {
        return $this->firstUnit;
    }

    public function setFirstUnit(int $firstUnit): self
    {
        $this->firstUnit = $firstUnit;

        return $this;
    }

    public function getFirstCurrency(): ?string
    {
        return $this->firstCurrency;
    }

    public function setFirstCurrency(string $firstCurrency): self
    {
        $this->firstCurrency = $firstCurrency;

        return $this;
    }

    public function getSecondUnit(): ?int
    {
        return $this->secondUnit;
    }

    public function setSecondUnit(int $secondUnit): self
    {
        $this->secondUnit = $secondUnit;

        return $this;
    }

    public function getSecondCurrency(): ?string
    {
        return $this->secondCurrency;
    }

    public function setSecondCurrency(string $secondCurrency): self
    {
        $this->secondCurrency = $secondCurrency;

        return $this;
    }

    public function getResult(): ?int
    {
        return $this->result;
    }

    public function setResult(int $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getResultCurrency(): ?string
    {
        return $this->resultCurrency;
    }

    public function setResultCurrency(string $resultCurrency): self
    {
        $this->resultCurrency = $resultCurrency;

        return $this;
    }
}
